

#ifndef __WALKING_PATTERN_GENERATOR_MANAGER__
#define __WALKING_PATTERN_GENERATOR_MANAGER__


#include <ros/ros.h>

#include <footStepPlannerManager_msgs/StepVectorStamped.h>

class WalkingPatternGeneratorManager {

private:
    ros::NodeHandle nHandle;
    ros::Subscriber footsteps_sub;
    ros::ServiceClient walkgen_client;
    ros::Publisher walkpath_pub;

public:

    WalkingPatternGeneratorManager();

    void footstepCallback(const footStepPlannerManager_msgs::StepVectorStampedConstPtr& msg);

};



#endif //__WALKING_PATTERN_GENERATOR_MANAGER__
