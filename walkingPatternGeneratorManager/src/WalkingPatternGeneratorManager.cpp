

#include "WalkingPatternGeneratorManager.hpp"

#include <halfsteps_pattern_generator/GetPath.h>


WalkingPatternGeneratorManager::WalkingPatternGeneratorManager():
/*    nHandle(),*/
    footsteps_sub(nHandle.subscribe<footStepPlannerManager_msgs::StepVectorStamped>("/footStepPlannerManager/stepVector_world",1, &WalkingPatternGeneratorManager::footstepCallback, this)),
    walkgen_client(nHandle.serviceClient<halfsteps_pattern_generator::GetPath>("/getPath")),
    walkpath_pub(nHandle.advertise<walk_msgs::WalkPath>("walkingPatternGeneratorManager/walk_trajectories",1000,1)){
}



void WalkingPatternGeneratorManager::footstepCallback(const footStepPlannerManager_msgs::StepVectorStampedConstPtr& msg){

    ROS_INFO("Received %u footprints.", (uint)msg->footsteps.size());

    halfsteps_pattern_generator::GetPath srv;


    if (!msg->footsteps.begin()->leg){ //means the first is a right foot

        ROS_INFO("Starting with the right foot.");
        srv.request.initial_right_foot_position.position.x = msg->footsteps[0].pose.x;
        srv.request.initial_right_foot_position.position.y = msg->footsteps[0].pose.y;
        srv.request.initial_right_foot_position.position.z = 0.0;
        srv.request.initial_right_foot_position.orientation.w = cos(.5*msg->footsteps[0].pose.theta);
        srv.request.initial_right_foot_position.orientation.x = 0.0;
        srv.request.initial_right_foot_position.orientation.y = 0.0;
        srv.request.initial_right_foot_position.orientation.z = sin(.5*msg->footsteps[0].pose.theta);

        srv.request.initial_left_foot_position.position.x = msg->footsteps[1].pose.x;
        srv.request.initial_left_foot_position.position.y = msg->footsteps[1].pose.y;
        srv.request.initial_left_foot_position.position.z = 0.0;
        srv.request.initial_left_foot_position.orientation.w = cos(.5*msg->footsteps[1].pose.theta);
        srv.request.initial_left_foot_position.orientation.x = 0.0;
        srv.request.initial_left_foot_position.orientation.y = 0.0;
        srv.request.initial_left_foot_position.orientation.z = sin(.5*msg->footsteps[1].pose.theta);
    } else {

        ROS_INFO("Starting with the left foot.");
        srv.request.initial_right_foot_position.position.x = msg->footsteps[1].pose.x;
        srv.request.initial_right_foot_position.position.y = msg->footsteps[1].pose.y;
        srv.request.initial_right_foot_position.position.z = 0.0;
        srv.request.initial_right_foot_position.orientation.w = cos(.5*msg->footsteps[1].pose.theta);
        srv.request.initial_right_foot_position.orientation.x = 0.0;
        srv.request.initial_right_foot_position.orientation.y = 0.0;
        srv.request.initial_right_foot_position.orientation.z = sin(.5*msg->footsteps[1].pose.theta);

        srv.request.initial_left_foot_position.position.x = msg->footsteps[0].pose.x;
        srv.request.initial_left_foot_position.position.y = msg->footsteps[0].pose.y;
        srv.request.initial_left_foot_position.position.z = 0.0;
        srv.request.initial_left_foot_position.orientation.w = cos(.5*msg->footsteps[0].pose.theta);
        srv.request.initial_left_foot_position.orientation.x = 0.0;
        srv.request.initial_left_foot_position.orientation.y = 0.0;
        srv.request.initial_left_foot_position.orientation.z = sin(.5*msg->footsteps[0].pose.theta);
    }


    if (!msg->footsteps.end()->leg){ //means the last is a right foot

        ROS_INFO("Finishing with the right foot.");

        srv.request.final_right_foot_position.position.x = msg->footsteps.end()->pose.x;
        srv.request.final_right_foot_position.position.y = msg->footsteps.end()->pose.y;
        srv.request.final_right_foot_position.position.z = 0.0;
        srv.request.final_right_foot_position.orientation.w = cos(.5*msg->footsteps.end()->pose.theta);
        srv.request.final_right_foot_position.orientation.x = 0.0;
        srv.request.final_right_foot_position.orientation.y = 0.0;
        srv.request.final_right_foot_position.orientation.z = sin(.5*msg->footsteps.end()->pose.theta);

        srv.request.final_left_foot_position.position.x = (msg->footsteps.end()-1)->pose.x;
        srv.request.final_left_foot_position.position.y = (msg->footsteps.end()-1)->pose.y;
        srv.request.final_left_foot_position.position.z = 0.0;
        srv.request.final_left_foot_position.orientation.w = cos(.5*(msg->footsteps.end()-1)->pose.theta);
        srv.request.final_left_foot_position.orientation.x = 0.0;
        srv.request.final_left_foot_position.orientation.y = 0.0;
        srv.request.final_left_foot_position.orientation.z = sin(.5*(msg->footsteps.end()-1)->pose.theta);
    } else { //means the last is a left foot

        ROS_INFO("Finishing with the left foot.");

        srv.request.final_right_foot_position.position.x = (msg->footsteps.end()-1)->pose.x;
        srv.request.final_right_foot_position.position.y = (msg->footsteps.end()-1)->pose.y;
        srv.request.final_right_foot_position.position.z = 0.0;
        srv.request.final_right_foot_position.orientation.w = cos(.5*(msg->footsteps.end()-1)->pose.theta);
        srv.request.final_right_foot_position.orientation.x = 0.0;
        srv.request.final_right_foot_position.orientation.y = 0.0;
        srv.request.final_right_foot_position.orientation.z = sin(.5*(msg->footsteps.end()-1)->pose.theta);

        srv.request.final_left_foot_position.position.x = msg->footsteps.end()->pose.x;
        srv.request.final_left_foot_position.position.y = msg->footsteps.end()->pose.y;
        srv.request.final_left_foot_position.position.z = 0.0;
        srv.request.final_left_foot_position.orientation.w = cos(.5*msg->footsteps.end()->pose.theta);
        srv.request.final_left_foot_position.orientation.x = 0.0;
        srv.request.final_left_foot_position.orientation.y = 0.0;
        srv.request.final_left_foot_position.orientation.z = sin(.5*msg->footsteps.end()->pose.theta);
    }

    srv.request.start_with_left_foot = msg->footsteps[0].leg;


    // fill in footprints
    srv.request.footprints.reserve(msg->footsteps.size());


    double slideUp, slideDown, horizontalDistance, stepHeight, robotHeight;
    if (!nHandle.getParam("slideUp", slideUp) ||
        !nHandle.getParam("slideDown", slideDown) ||
        !nHandle.getParam("horizontalDistance", horizontalDistance) ||
        !nHandle.getParam("stepHeight", stepHeight) ||
        !nHandle.getParam("robotHeight", robotHeight)){
        ROS_WARN("Using default parameters.");
        slideUp = -.5;//-0.5;
        slideDown = -.5;//-0.5;
        horizontalDistance = 0.05;//0.19;
        stepHeight = 0.05;
        robotHeight = .8;
    }

    halfsteps_pattern_generator::Footprint footPrint;
    footPrint.footprint.beginTime.sec = 0;
    footPrint.footprint.beginTime.nsec = 0;
    footPrint.footprint.duration.sec = 0.;
    footPrint.footprint.duration.nsec = 1. * 1e9;
    footPrint.slideUp = slideUp;
    footPrint.slideDown = slideDown;
    footPrint.horizontalDistance = horizontalDistance;
    footPrint.stepHeight = stepHeight;

    for (uint i = 2; i<msg->footsteps.size()-2; ++i){
        footPrint.footprint.x = msg->footsteps[i].pose.x;
        footPrint.footprint.y = msg->footsteps[i].pose.y;
        footPrint.footprint.theta = msg->footsteps[i].pose.theta;
        srv.request.footprints.push_back(footPrint);
    }


    // set initial com position between the two feet
    srv.request.initial_center_of_mass_position.x = (msg->footsteps[0].pose.x + msg->footsteps[1].pose.x)/2.0;
    srv.request.initial_center_of_mass_position.y = (msg->footsteps[0].pose.y + msg->footsteps[1].pose.y)/2.0;
    srv.request.initial_center_of_mass_position.z = robotHeight;

    ROS_INFO("Sending request to the walking pattern generator with %u steps.", (uint)srv.request.footprints.size());


    if (walkgen_client.call(srv)){
        ROS_INFO("Received path");

        ROS_INFO("COM Path size: %d", srv.response.path.center_of_mass.points.size());
        ROS_INFO("ZMP Path size: %d", srv.response.path.zmp.points.size());
        ROS_INFO("LFoot Path size: %d", srv.response.path.left_foot.poses.size());
        ROS_INFO("RFoot Path size: %d", srv.response.path.right_foot.poses.size());

        walkpath_pub.publish(srv.response.path);
    } else {
        ROS_ERROR("Unable to receive path");
    }

}
