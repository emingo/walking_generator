

#include "WalkingPatternGeneratorManager.hpp"
#include <ros/ros.h>


int main(int argc, char* argv[])
{
    ros::init(argc,argv, "wp_generator");

    WalkingPatternGeneratorManager manager;

    ROS_INFO("Done launching wp_generator");
    ros::spin();
    ros::waitForShutdown();

    // shutdown
    ros::shutdown();

    ROS_INFO("Have a nice day!");
    return 0;
}
