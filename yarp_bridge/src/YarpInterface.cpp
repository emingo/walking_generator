

#include "YarpInterface.hpp"
#include <math.h>
#include <tf/tf.h>

YarpInterface::YarpInterface():
    trajSub_(nh_.subscribe("walkingPatternGeneratorManager/walk_trajectories",1000,&YarpInterface::convert_and_resendCB,this)),
    trajPub_(nh_.advertise<yarp_bridge::yarp_walking_trj>("iCub_bridge/walk_trajectories",1000,1)),
    trajStepPub_(nh_.advertise<yarp_bridge::yarp_walking_trj>("iCub_bridge/walk_traj_step",1000,1)),
    curr_index_(0)
{

}



const char stance_foot[] = "rld";
const double eps = 0.0001;



void YarpInterface::convert_and_resendCB(const walk_msgs::WalkPath& wp) {

    ROS_INFO("Recived a Trajectory...");

    tf::Transform ZTransform(tf::createQuaternionFromYaw(M_PI),tf::Vector3(0.0, 0.0, 0.0));
//    yarp_bridge::yarp_walking_trj yarp_traj_rel;

    const /*unsigned*/ int n_points = wp.center_of_mass.points.size();
    ROS_INFO("...of %d points...", n_points);

    yarp_traj_.COM_x.reserve(n_points);
    yarp_traj_.COM_y.reserve(n_points);
    yarp_traj_.COM_z.reserve(n_points);
    yarp_traj_.ZMP_x.reserve(n_points);
    yarp_traj_.ZMP_y.reserve(n_points);
    yarp_traj_.left_foot_x.reserve(n_points);
    yarp_traj_.left_foot_y.reserve(n_points);
    yarp_traj_.left_foot_z.reserve(n_points);
    yarp_traj_.left_foot_roll.reserve(n_points);
    yarp_traj_.left_foot_pitch.reserve(n_points);
    yarp_traj_.left_foot_yaw.reserve(n_points);
    yarp_traj_.right_foot_x.reserve(n_points);
    yarp_traj_.right_foot_y.reserve(n_points);
    yarp_traj_.right_foot_z.reserve(n_points);
    yarp_traj_.right_foot_roll.reserve(n_points);
    yarp_traj_.right_foot_pitch.reserve(n_points);
    yarp_traj_.right_foot_yaw.reserve(n_points);

    yarp_traj_.swing_foot_x.reserve(n_points);
    yarp_traj_.swing_foot_y.reserve(n_points);
    yarp_traj_.swing_foot_z.reserve(n_points);
    yarp_traj_.swing_foot_roll.reserve(n_points);
    yarp_traj_.swing_foot_pitch.reserve(n_points);
    yarp_traj_.swing_foot_yaw.reserve(n_points);

    yarp_traj_.stance_foot.reserve(n_points);

    // TODO: remove these copies which cause useless overhead
    std::vector<geometry_msgs::PointStamped> CoM_points = wp.center_of_mass.points;
    std::vector<walk_msgs::Point2dStamped> ZMP_points = wp.zmp.points;
    std::vector<geometry_msgs::PoseStamped> Lfoot_poses = wp.left_foot.poses;
    std::vector<geometry_msgs::PoseStamped> Rfoot_poses = wp.right_foot.poses;

    tf::Pose fl, fr;
    double roll, pitch, yaw;
    for(int i = 0; i < n_points; ++i)
    {
        yarp_traj_.COM_x.push_back(CoM_points[i].point.x);
        yarp_traj_.COM_y.push_back(CoM_points[i].point.y);
        yarp_traj_.COM_z.push_back(CoM_points[i].point.z);

        yarp_traj_.COM_dx.push_back(
                (-CoM_points[std::min(i+2,n_points-1)].point.x
                 +8.0*CoM_points[std::min(i+1,n_points-1)].point.x
                 -8.0*CoM_points[std::max(i-1,0)].point.x
                 +CoM_points[std::max(i-2,0)].point.x)/(12.0*time_step_));

        yarp_traj_.COM_dy.push_back(
                (-CoM_points[std::min(i+2,n_points-1)].point.y
                 +8.0*CoM_points[std::min(i+1,n_points-1)].point.y
                 -8.0*CoM_points[std::max(i-1,0)].point.y
                 +CoM_points[std::max(i-2,0)].point.y)/(12.0*time_step_));

        yarp_traj_.COM_dz.push_back(
                (-CoM_points[std::min(i+2,n_points-1)].point.z
                 +8.0*CoM_points[std::min(i+1,n_points-1)].point.z
                 -8.0*CoM_points[std::max(i-1,0)].point.z
                 +CoM_points[std::max(i-2,0)].point.z)/(12.0*time_step_));



        yarp_traj_.ZMP_x.push_back(ZMP_points[i].point.x);
        yarp_traj_.ZMP_y.push_back(ZMP_points[i].point.y);

        /* To Poses we apply the ZTransform */
        tf::poseMsgToTF(Lfoot_poses[i].pose, fl);
        tf::poseMsgToTF(Rfoot_poses[i].pose, fr);

        fl = fl*ZTransform;
        fr = fr*ZTransform;

        yarp_traj_.left_foot_x.push_back(fl.getOrigin().getX());
        yarp_traj_.left_foot_y.push_back(fl.getOrigin().getY());
        yarp_traj_.left_foot_z.push_back(fl.getOrigin().getZ());
        tf::Matrix3x3(fl.getRotation()).getRPY(roll, pitch, yaw);
        yarp_traj_.left_foot_roll.push_back( (float)roll );
        yarp_traj_.left_foot_pitch.push_back( (float)pitch );
        yarp_traj_.left_foot_yaw.push_back( (float)yaw );

        yarp_traj_.right_foot_x.push_back(fr.getOrigin().getX());
        yarp_traj_.right_foot_y.push_back(fr.getOrigin().getY());
        yarp_traj_.right_foot_z.push_back(fr.getOrigin().getZ());
        tf::Matrix3x3(fr.getRotation()).getRPY(roll, pitch, yaw);
        yarp_traj_.right_foot_roll.push_back( (float)roll );
        yarp_traj_.right_foot_pitch.push_back( (float)pitch );
        yarp_traj_.right_foot_yaw.push_back( (float)yaw );



        tf::Pose st2sw;
        if(yarp_traj_.right_foot_z[i] <= eps){
            st2sw = fr.inverse()*fl;
            if (yarp_traj_.left_foot_z[i] <= eps){ // double support
                yarp_traj_.stance_foot.push_back(stance_foot[2]);
            } else { // right support
                yarp_traj_.stance_foot.push_back(stance_foot[0]);
            }
        } else if (yarp_traj_.left_foot_z[i] <= eps){
            st2sw = fl.inverse()*fr;
            yarp_traj_.stance_foot.push_back(stance_foot[1]);
        } else {
            ROS_ERROR("No support leg. This should never happen!");
        }

        yarp_traj_.swing_foot_x.push_back(st2sw.getOrigin().getX());
        yarp_traj_.swing_foot_y.push_back(st2sw.getOrigin().getY());
        yarp_traj_.swing_foot_z.push_back(st2sw.getOrigin().getZ());
        tf::Matrix3x3(st2sw.getRotation()).getRPY(roll, pitch, yaw);
        yarp_traj_.swing_foot_roll.push_back( (float)roll );
        yarp_traj_.swing_foot_pitch.push_back( (float)pitch );
        yarp_traj_.swing_foot_yaw.push_back( (float)yaw );

    }

    trajPub_.publish(yarp_traj_);

    curr_index_ = 0;

    ROS_INFO("...Trajectories converted and published!");
}


void YarpInterface::send_step() {

    if (yarp_traj_.COM_dx.size() <= curr_index_){
        return;
    }


    if (curr_index_ == 0){
        ROS_INFO("Start broadcasting a new trajectory.");
    } else if (!(curr_index_%(uint)(1.0/time_step_))) {
        ROS_INFO("%2.2f%% of the trajectory transmitted.", (double)curr_index_/yarp_traj_.COM_dx.size()*100);
    } else if (curr_index_ == yarp_traj_.COM_dx.size()-1){
        ROS_INFO("Done broadcasting trajectory.");
    }


    yarp_bridge::yarp_walking_trj yarp_traj_curr;

    yarp_traj_curr.COM_x.push_back(yarp_traj_.COM_x[curr_index_]);
    yarp_traj_curr.COM_y.push_back(yarp_traj_.COM_y[curr_index_]);
    yarp_traj_curr.COM_z.push_back(yarp_traj_.COM_z[curr_index_]);

    yarp_traj_curr.COM_dx.push_back(yarp_traj_.COM_dx[curr_index_]);
    yarp_traj_curr.COM_dy.push_back(yarp_traj_.COM_dy[curr_index_]);
    yarp_traj_curr.COM_dz.push_back(yarp_traj_.COM_dz[curr_index_]);

    yarp_traj_curr.ZMP_x.push_back(yarp_traj_.ZMP_x[curr_index_]);
    yarp_traj_curr.ZMP_y.push_back(yarp_traj_.ZMP_y[curr_index_]);

    yarp_traj_curr.left_foot_x.push_back(yarp_traj_.left_foot_x[curr_index_]);
    yarp_traj_curr.left_foot_y.push_back(yarp_traj_.left_foot_y[curr_index_]);
    yarp_traj_curr.left_foot_z.push_back(yarp_traj_.left_foot_z[curr_index_]);
    yarp_traj_curr.left_foot_roll.push_back(yarp_traj_.left_foot_roll[curr_index_]);
    yarp_traj_curr.left_foot_pitch.push_back(yarp_traj_.left_foot_pitch[curr_index_]);
    yarp_traj_curr.left_foot_yaw.push_back(yarp_traj_.left_foot_yaw[curr_index_]);

    yarp_traj_curr.right_foot_x.push_back(yarp_traj_.right_foot_x[curr_index_]);
    yarp_traj_curr.right_foot_y.push_back(yarp_traj_.right_foot_y[curr_index_]);
    yarp_traj_curr.right_foot_z.push_back(yarp_traj_.right_foot_z[curr_index_]);
    yarp_traj_curr.right_foot_roll.push_back(yarp_traj_.right_foot_roll[curr_index_]);
    yarp_traj_curr.right_foot_pitch.push_back(yarp_traj_.right_foot_pitch[curr_index_]);
    yarp_traj_curr.right_foot_yaw.push_back(yarp_traj_.right_foot_yaw[curr_index_]);

    yarp_traj_curr.swing_foot_x.push_back(yarp_traj_.swing_foot_x[curr_index_]);
    yarp_traj_curr.swing_foot_y.push_back(yarp_traj_.swing_foot_y[curr_index_]);
    yarp_traj_curr.swing_foot_z.push_back(yarp_traj_.swing_foot_z[curr_index_]);
    yarp_traj_curr.swing_foot_roll.push_back(yarp_traj_.swing_foot_roll[curr_index_]);
    yarp_traj_curr.swing_foot_pitch.push_back(yarp_traj_.swing_foot_pitch[curr_index_]);
    yarp_traj_curr.swing_foot_yaw.push_back(yarp_traj_.swing_foot_yaw[curr_index_]);

    yarp_traj_curr.stance_foot.push_back(yarp_traj_.stance_foot[curr_index_]);

    curr_index_++;

    trajStepPub_.publish(yarp_traj_curr);

}


void YarpInterface::spin(){

    if (!nh_.getParam("time_step", time_step_)){
        time_step_ = 20e-3;
        ROS_WARN("Using default time_step.");
    }

    ros::Rate rate(1.0/time_step_);

    while(ros::ok()){
        send_step();
        ros::spinOnce();
        rate.sleep();
    }
}
