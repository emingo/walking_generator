#include <ros/ros.h>
#include "YarpInterface.hpp"


int main(int argc, char **argv)
{
    ros::init(argc, argv, "iCub_bridge");

    YarpInterface interface;
    ROS_INFO("iCub Bridge Start!");
    interface.spin();

	return 0;
}
