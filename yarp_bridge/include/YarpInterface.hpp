

#ifndef __YARP_INTERFACE__
#define __YARP_INTERFACE__

#include <ros/ros.h>
#include <walk_msgs/WalkPath.h>

#include <yarp_bridge/yarp_walking_trj.h>

class YarpInterface {

private:
    ros::NodeHandle nh_;
    ros::Subscriber trajSub_;
    ros::Publisher trajPub_;
    ros::Publisher trajStepPub_;

    yarp_bridge::yarp_walking_trj yarp_traj_;
    unsigned int curr_index_;

    double time_step_;

    void send_step();
public:
    YarpInterface();
    void convert_and_resendCB(const walk_msgs::WalkPath& wp);
    void spin();
};

#endif //__YARP_INTERFACE__
