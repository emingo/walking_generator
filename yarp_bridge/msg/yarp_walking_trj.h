// This is an automatically generated file.
// Generated from this yarp_walking_trj.msg definition:
//   float32[] COM_x
//   float32[] COM_y
//   float32[] COM_z
//   
//   float32[] ZMP_x
//   float32[] ZMP_y
//   
//   float32[] left_foot_x
//   float32[] left_foot_y
//   float32[] left_foot_z
//   float32[] left_foot_roll
//   float32[] left_foot_pitch
//   float32[] left_foot_yaw
//   
//   float32[] right_foot_x
//   float32[] right_foot_y
//   float32[] right_foot_z
//   float32[] right_foot_roll
//   float32[] right_foot_pitch
//   float32[] right_foot_yaw
// Instances of this class can be read and written with YARP ports,
// using a ROS-compatible format.

#ifndef YARPMSG_TYPE_yarp_walking_trj
#define YARPMSG_TYPE_yarp_walking_trj

#include <string>
#include <vector>
#include <yarp/os/Portable.h>
#include <yarp/os/ConstString.h>
#include <yarp/os/NetInt16.h>
#include <yarp/os/NetUint16.h>
#include <yarp/os/NetInt32.h>
#include <yarp/os/NetUint32.h>
#include <yarp/os/NetInt64.h>
#include <yarp/os/NetUint64.h>
#include <yarp/os/NetFloat32.h>
#include <yarp/os/NetFloat64.h>

class yarp_walking_trj : public yarp::os::Portable {
public:
  yarp::os::ConstString getTypeName() const {
    return "yarp_walking_trj";
  }

  std::vector<yarp::os::NetFloat32> COM_x;
  std::vector<yarp::os::NetFloat32> COM_y;
  std::vector<yarp::os::NetFloat32> COM_z;
  std::vector<yarp::os::NetFloat32> ZMP_x;
  std::vector<yarp::os::NetFloat32> ZMP_y;
  std::vector<yarp::os::NetFloat32> left_foot_x;
  std::vector<yarp::os::NetFloat32> left_foot_y;
  std::vector<yarp::os::NetFloat32> left_foot_z;
  std::vector<yarp::os::NetFloat32> left_foot_roll;
  std::vector<yarp::os::NetFloat32> left_foot_pitch;
  std::vector<yarp::os::NetFloat32> left_foot_yaw;
  std::vector<yarp::os::NetFloat32> right_foot_x;
  std::vector<yarp::os::NetFloat32> right_foot_y;
  std::vector<yarp::os::NetFloat32> right_foot_z;
  std::vector<yarp::os::NetFloat32> right_foot_roll;
  std::vector<yarp::os::NetFloat32> right_foot_pitch;
  std::vector<yarp::os::NetFloat32> right_foot_yaw;

  bool read(yarp::os::ConnectionReader& connection) {
    // *** COM_x ***
    int len = connection.expectInt();
    COM_x.resize(len);
    if (!connection.expectBlock((char*)&COM_x[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** COM_y ***
    len = connection.expectInt();
    COM_y.resize(len);
    if (!connection.expectBlock((char*)&COM_y[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** COM_z ***
    len = connection.expectInt();
    COM_z.resize(len);
    if (!connection.expectBlock((char*)&COM_z[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** ZMP_x ***
    len = connection.expectInt();
    ZMP_x.resize(len);
    if (!connection.expectBlock((char*)&ZMP_x[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** ZMP_y ***
    len = connection.expectInt();
    ZMP_y.resize(len);
    if (!connection.expectBlock((char*)&ZMP_y[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** left_foot_x ***
    len = connection.expectInt();
    left_foot_x.resize(len);
    if (!connection.expectBlock((char*)&left_foot_x[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** left_foot_y ***
    len = connection.expectInt();
    left_foot_y.resize(len);
    if (!connection.expectBlock((char*)&left_foot_y[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** left_foot_z ***
    len = connection.expectInt();
    left_foot_z.resize(len);
    if (!connection.expectBlock((char*)&left_foot_z[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** left_foot_roll ***
    len = connection.expectInt();
    left_foot_roll.resize(len);
    if (!connection.expectBlock((char*)&left_foot_roll[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** left_foot_pitch ***
    len = connection.expectInt();
    left_foot_pitch.resize(len);
    if (!connection.expectBlock((char*)&left_foot_pitch[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** left_foot_yaw ***
    len = connection.expectInt();
    left_foot_yaw.resize(len);
    if (!connection.expectBlock((char*)&left_foot_yaw[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** right_foot_x ***
    len = connection.expectInt();
    right_foot_x.resize(len);
    if (!connection.expectBlock((char*)&right_foot_x[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** right_foot_y ***
    len = connection.expectInt();
    right_foot_y.resize(len);
    if (!connection.expectBlock((char*)&right_foot_y[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** right_foot_z ***
    len = connection.expectInt();
    right_foot_z.resize(len);
    if (!connection.expectBlock((char*)&right_foot_z[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** right_foot_roll ***
    len = connection.expectInt();
    right_foot_roll.resize(len);
    if (!connection.expectBlock((char*)&right_foot_roll[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** right_foot_pitch ***
    len = connection.expectInt();
    right_foot_pitch.resize(len);
    if (!connection.expectBlock((char*)&right_foot_pitch[0],sizeof(yarp::os::NetFloat32)*len)) return false;

    // *** right_foot_yaw ***
    len = connection.expectInt();
    right_foot_yaw.resize(len);
    if (!connection.expectBlock((char*)&right_foot_yaw[0],sizeof(yarp::os::NetFloat32)*len)) return false;
    return !connection.isError();
  }

  bool write(yarp::os::ConnectionWriter& connection) {
    // *** COM_x ***
    connection.appendInt(COM_x.size());
    connection.appendExternalBlock((char*)&COM_x[0],sizeof(yarp::os::NetFloat32)*COM_x.size());

    // *** COM_y ***
    connection.appendInt(COM_y.size());
    connection.appendExternalBlock((char*)&COM_y[0],sizeof(yarp::os::NetFloat32)*COM_y.size());

    // *** COM_z ***
    connection.appendInt(COM_z.size());
    connection.appendExternalBlock((char*)&COM_z[0],sizeof(yarp::os::NetFloat32)*COM_z.size());

    // *** ZMP_x ***
    connection.appendInt(ZMP_x.size());
    connection.appendExternalBlock((char*)&ZMP_x[0],sizeof(yarp::os::NetFloat32)*ZMP_x.size());

    // *** ZMP_y ***
    connection.appendInt(ZMP_y.size());
    connection.appendExternalBlock((char*)&ZMP_y[0],sizeof(yarp::os::NetFloat32)*ZMP_y.size());

    // *** left_foot_x ***
    connection.appendInt(left_foot_x.size());
    connection.appendExternalBlock((char*)&left_foot_x[0],sizeof(yarp::os::NetFloat32)*left_foot_x.size());

    // *** left_foot_y ***
    connection.appendInt(left_foot_y.size());
    connection.appendExternalBlock((char*)&left_foot_y[0],sizeof(yarp::os::NetFloat32)*left_foot_y.size());

    // *** left_foot_z ***
    connection.appendInt(left_foot_z.size());
    connection.appendExternalBlock((char*)&left_foot_z[0],sizeof(yarp::os::NetFloat32)*left_foot_z.size());

    // *** left_foot_roll ***
    connection.appendInt(left_foot_roll.size());
    connection.appendExternalBlock((char*)&left_foot_roll[0],sizeof(yarp::os::NetFloat32)*left_foot_roll.size());

    // *** left_foot_pitch ***
    connection.appendInt(left_foot_pitch.size());
    connection.appendExternalBlock((char*)&left_foot_pitch[0],sizeof(yarp::os::NetFloat32)*left_foot_pitch.size());

    // *** left_foot_yaw ***
    connection.appendInt(left_foot_yaw.size());
    connection.appendExternalBlock((char*)&left_foot_yaw[0],sizeof(yarp::os::NetFloat32)*left_foot_yaw.size());

    // *** right_foot_x ***
    connection.appendInt(right_foot_x.size());
    connection.appendExternalBlock((char*)&right_foot_x[0],sizeof(yarp::os::NetFloat32)*right_foot_x.size());

    // *** right_foot_y ***
    connection.appendInt(right_foot_y.size());
    connection.appendExternalBlock((char*)&right_foot_y[0],sizeof(yarp::os::NetFloat32)*right_foot_y.size());

    // *** right_foot_z ***
    connection.appendInt(right_foot_z.size());
    connection.appendExternalBlock((char*)&right_foot_z[0],sizeof(yarp::os::NetFloat32)*right_foot_z.size());

    // *** right_foot_roll ***
    connection.appendInt(right_foot_roll.size());
    connection.appendExternalBlock((char*)&right_foot_roll[0],sizeof(yarp::os::NetFloat32)*right_foot_roll.size());

    // *** right_foot_pitch ***
    connection.appendInt(right_foot_pitch.size());
    connection.appendExternalBlock((char*)&right_foot_pitch[0],sizeof(yarp::os::NetFloat32)*right_foot_pitch.size());

    // *** right_foot_yaw ***
    connection.appendInt(right_foot_yaw.size());
    connection.appendExternalBlock((char*)&right_foot_yaw[0],sizeof(yarp::os::NetFloat32)*right_foot_yaw.size());
    return !connection.isError();
  }
};

#endif
