# walking_generator

# footStepPlannerManager
Permits to specify a goal in world frame and it computes a set of footsteps that can be used with a walking pattern generator.

# footStepPlannerManager_msgs
Specific mesages for the footStepPlannerManager 

# walkingPatternGeneratorManager
Giver a set of footsteps it computes the trajectories of COM, ZMP, left and right feet.

# yarp_bridge
Read trajectories and computes relative transformation between stance foot and swing foot.
