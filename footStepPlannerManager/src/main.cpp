#include <ros/ros.h>
#include <footStepPlannerManager/fspm_goal.h>
#include <humanoid_nav_msgs/PlanFootsteps.h>
#include <humanoid_nav_msgs/StepTarget.h>
#include <footStepPlannerManager_msgs/StepVectorStamped.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <sstream>

char leg[] = "RL";

class footStepPlanner
{
public:
    footStepPlanner():
        _n(),
        _service(_n.advertiseService("footStepPlannerManager/goal_world", &footStepPlanner::setFootStepPlannerGoal, this)),
        _start(),
        _goal(),
        _client(_n.serviceClient<humanoid_nav_msgs::PlanFootsteps>("plan_footsteps")),
        _feet_msg(),
        _step_pub(_n.advertise<footStepPlannerManager_msgs::StepVectorStamped>("footStepPlannerManager/stepVector_world",1000,1)),
        _step_pub_relative(_n.advertise<footStepPlannerManager_msgs::StepVectorStamped>("footStepPlannerManager/stepVector_relative",1000,1)),
        _T(),
        _map_to_world()
    {
        if(!_n.getParam("footStepPlanner/start/x", _start.x) ||
           !_n.getParam("footStepPlanner/start/y", _start.y) ||
           !_n.getParam("footStepPlanner/start/theta", _start.theta))
        {
            ROS_WARN("Using default parameters.");
            _start.x = 0.0;
            _start.y = 0.0;
            _start.theta = 0.0;
            _n.setParam("footStepPlanner/start/x", _start.x);
            _n.setParam("footStepPlanner/start/y", _start.y);
            _n.setParam("footStepPlanner/start/theta", _start.theta);
        }
        _goal.x = 0.0;
        _goal.y = 0.0;
        _goal.theta = 0.0;

        _T.setOrigin(tf::Vector3(_start.x, _start.y, 0.0));
        _T.setRotation(tf::createQuaternionFromYaw(_start.theta));
    }

    geometry_msgs::Pose2D getStart(){return _start;}
    geometry_msgs::Pose2D getGoal(){return _goal;}
    void publishMapToWorldTransform()
    {
        _map_to_world.sendTransform(tf::StampedTransform(_T, ros::Time::now(), "/map", "/world"));
    }

private:
    ros::NodeHandle _n;
    ros::ServiceServer _service;
    geometry_msgs::Pose2D _start;
    geometry_msgs::Pose2D _goal;
    ros::ServiceClient _client;
    footStepPlannerManager_msgs::StepVectorStamped _feet_msg;
    ros::Publisher _step_pub;
    ros::Publisher _step_pub_relative;
    tf::Transform _T;
    tf::TransformBroadcaster _map_to_world;


    bool setFootStepPlannerGoal(footStepPlannerManager::fspm_goalRequest &req,
                                footStepPlannerManager::fspm_goalResponse &res)
    {
        ROS_INFO("...new request GOAL = [%f, %f, %f] recived...", req.goal.x, req.goal.y, req.goal.theta);

        _goal.x = req.goal.x;
        _goal.y = req.goal.y;
        _goal.theta = req.goal.theta;

        humanoid_nav_msgs::PlanFootsteps srv;
        srv.request.start = _start;
        srv.request.goal = _goal;

        bool result;
        if (_client.call(srv))
        {
            if(srv.response.result)
            {
                _feet_msg.header.stamp = ros::Time::now();

                humanoid_nav_msgs::StepTarget step0;
                step0.leg = (srv.response.footsteps[0].leg+1)%2;
                step0.pose.x = 2.0 * _start.x - srv.response.footsteps[0].pose.x;
                step0.pose.y = 2.0 * _start.y - srv.response.footsteps[0].pose.y;
                step0.pose.theta = 2.0 * _start.theta - srv.response.footsteps[0].pose.theta;
                //std::cout<<"First Step "<<step0.pose.x<<" "<<step0.pose.y<<" "<<step0.pose.theta<<std::endl;

                _feet_msg.footsteps.clear();
                _feet_msg.footsteps.reserve(srv.response.footsteps.size()+1);
                //std::cout<<"srv size "<<srv.response.footsteps.size()<<std::endl;
                _feet_msg.footsteps.push_back(step0);

                for(unsigned int i = 0; i < srv.response.footsteps.size(); ++i)
                    _feet_msg.footsteps.push_back(srv.response.footsteps[i]);
                //std::cout<<"footsteps size "<<feet_msg.footsteps.size()<<std::endl;

                computeRelativeTransformations(_feet_msg);
                _step_pub.publish(_feet_msg);
                ROS_INFO("...new goal set to planner!");
                result = true;
            #ifdef USE_TF
                first_request = result;
            #endif
                _T.setOrigin(tf::Vector3(_start.x, _start.y, 0.0));
                _T.setRotation(tf::createQuaternionFromYaw(_start.theta));
                _start = _goal;
                _n.setParam("footStepPlanner/start/x", _start.x);
                _n.setParam("footStepPlanner/start/y", _start.y);
                _n.setParam("footStepPlanner/start/theta", _start.theta);
            } else {result = srv.response.result;}
        }
        else
        {
            ROS_ERROR("...failed to set new goal to planner!");
            result = false;
        }
        ROS_INFO("New starting Pose of the robot is: [x: %f, y: %f, theta: %f]", _start.x, _start.y, _start.theta);
        ROS_INFO("Ready for a new request...");

        res.result = result;
        return res.result;
    }


    void computeRelativeTransformations(const footStepPlannerManager_msgs::StepVectorStamped& footsteps_world)
    {
        footStepPlannerManager_msgs::StepVectorStamped footsteps_relative;
        footsteps_relative.footsteps.reserve(footsteps_world.footsteps.size()-1);

        tf::Transform T_i; // T0
        tf::Transform T_j; // T1
        for(unsigned int i = 0; i < footsteps_world.footsteps.size()-1; ++i)
        {
            T_i.setOrigin(tf::Vector3(footsteps_world.footsteps[i].pose.x, footsteps_world.footsteps[i].pose.y, 0.0));
            T_i.setRotation(tf::createQuaternionFromYaw(footsteps_world.footsteps[i].pose.theta));

            T_j.setOrigin(tf::Vector3(footsteps_world.footsteps[i+1].pose.x, footsteps_world.footsteps[i+1].pose.y, 0.0));
            T_j.setRotation(tf::createQuaternionFromYaw(footsteps_world.footsteps[i+1].pose.theta));

            tf::Transform T_relative = T_i.inverse() * T_j;

            humanoid_nav_msgs::StepTarget step;
            step.pose.x = T_relative.getOrigin().getX();
            step.pose.y = T_relative.getOrigin().getY();
            step.pose.theta = tf::getYaw(T_relative.getRotation());
            step.leg = footsteps_world.footsteps[i].leg;

            footsteps_relative.footsteps.push_back(step);
        }
        footsteps_relative.header.stamp = footsteps_world.header.stamp;
        _step_pub_relative.publish(footsteps_relative);
    }
};

#ifdef USE_TF
bool first_request = false;

void sendFeetToTf_world(const std::vector<humanoid_nav_msgs::StepTarget>& footsteps)
{
    static tf::TransformBroadcaster br;
    std::vector<tf::Transform> feet_transforms;
    feet_transforms.reserve(footsteps.size());

    std::vector<std::string> names;
    names.reserve(footsteps.size());

    for(uint step_id = 0; step_id < footsteps.size(); ++step_id)
    {
        humanoid_nav_msgs::StepTarget step = footsteps[step_id];

        std::stringstream name;
        name<<"step"<<leg[step.leg]<<step_id;
        names.push_back(name.str());

        tf::Transform T;
        T.setOrigin(tf::Vector3(step.pose.x, step.pose.y, 0.0));
        T.setRotation(tf::createQuaternionFromYaw(step.pose.theta));
        feet_transforms.push_back(T);
    }
    ros::Time t = ros::Time::now();

    for(uint i = 0; i < feet_transforms.size(); ++i)
        br.sendTransform(tf::StampedTransform(feet_transforms[i], t, "map", names[i]));
}
#endif





int main(int argc, char **argv)
{
    ros::init(argc, argv, "footStepPlannerManager");
    footStepPlanner fp;

    ROS_INFO("FOOT STEP PLANNER MANAGER");
    ROS_INFO("Starting Pose of the robot is: [x: %f, y: %f, theta: %f]", fp.getStart().x, fp.getStart().y, fp.getStart().theta);
    ROS_INFO("Ready for a new request...");

    ros::Rate rate(100);

    while(ros::ok())
    {
#ifdef USE_TF
        if(first_request)
            sendFeetToTf_world(feet_msg.footsteps);
#endif

        fp.publishMapToWorldTransform();
        ros::spinOnce();
        rate.sleep();
    }


	return 0;
}
