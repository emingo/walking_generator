// This is an automatically generated file.
// Generated from this geometry_msgs_Pose2D.msg definition:
//   # This expresses a position and orientation on a 2D manifold.
//   
//   float64 x
//   float64 y
//   float64 theta
// Instances of this class can be read and written with YARP ports,
// using a ROS-compatible format.

#ifndef YARPMSG_TYPE_geometry_msgs_Pose2D
#define YARPMSG_TYPE_geometry_msgs_Pose2D

#include <string>
#include <vector>
#include <yarp/os/Portable.h>
#include <yarp/os/ConstString.h>
#include <yarp/os/NetInt16.h>
#include <yarp/os/NetUint16.h>
#include <yarp/os/NetInt32.h>
#include <yarp/os/NetUint32.h>
#include <yarp/os/NetInt64.h>
#include <yarp/os/NetUint64.h>
#include <yarp/os/NetFloat32.h>
#include <yarp/os/NetFloat64.h>
#include <TickTime.h>
#include <std_msgs/Header.h>

class geometry_msgs_Pose2D : public yarp::os::Portable {
public:
  yarp::os::ConstString getTypeName() const {
    return "geometry_msgs_Pose2D";
  }

  yarp::os::NetFloat64 x;
  yarp::os::NetFloat64 y;
  yarp::os::NetFloat64 theta;

  bool read(yarp::os::ConnectionReader& connection) {
    // *** x ***
    x = connection.expectDouble();

    // *** y ***
    y = connection.expectDouble();

    // *** theta ***
    theta = connection.expectDouble();
    return !connection.isError();
  }

  bool write(yarp::os::ConnectionWriter& connection) {
    // *** x ***
    connection.appendDouble(x);

    // *** y ***
    connection.appendDouble(y);

    // *** theta ***
    connection.appendDouble(theta);
    return !connection.isError();
  }
};

#endif
