// This is an automatically generated file.
// Generated from this StepVectorStamped.msg definition:
//   
//   std_msgs/Header header
//   humanoid_nav_msgs/StepTarget[] footsteps
// Instances of this class can be read and written with YARP ports,
// using a ROS-compatible format.

#ifndef YARPMSG_TYPE_StepVectorStamped
#define YARPMSG_TYPE_StepVectorStamped

#include <string>
#include <vector>
#include <yarp/os/Portable.h>
#include <yarp/os/ConstString.h>
#include <yarp/os/NetInt16.h>
#include <yarp/os/NetUint16.h>
#include <yarp/os/NetInt32.h>
#include <yarp/os/NetUint32.h>
#include <yarp/os/NetInt64.h>
#include <yarp/os/NetUint64.h>
#include <yarp/os/NetFloat32.h>
#include <yarp/os/NetFloat64.h>
#include <TickTime.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/Pose2D.h>
#include <humanoid_nav_msgs/StepTarget.h>

class StepVectorStamped : public yarp::os::Portable {
public:
  yarp::os::ConstString getTypeName() const {
    return "StepVectorStamped";
  }

  std_msgs/Header header;
  std::vector<humanoid_nav_msgs/StepTarget> footsteps;

  bool read(yarp::os::ConnectionReader& connection) {
    // *** header ***
    if (!header.read(connection)) return false;

    // *** footsteps ***
    int len = connection.expectInt();
    footsteps.resize(len);
    for (int i=0; i<len; i++) {
      if (!footsteps[i].read(connection)) return false;
    }
    return !connection.isError();
  }

  bool write(yarp::os::ConnectionWriter& connection) {
    // *** header ***
    if (!header.write(connection)) return false;

    // *** footsteps ***
    connection.appendInt(footsteps.size());
    for (int i=0; i<footsteps.size(); i++) {
      if (!footsteps[i].write(connection)) return false;
    }
    return !connection.isError();
  }
};

#endif
