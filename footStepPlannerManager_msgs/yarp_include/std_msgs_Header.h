// This is an automatically generated file.
// Generated from this std_msgs_Header.msg definition:
//   # Standard metadata for higher-level stamped data types.
//   # This is generally used to communicate timestamped data 
//   # in a particular coordinate frame.
//   # 
//   # sequence ID: consecutively increasing ID 
//   uint32 seq
//   #Two-integer timestamp that is expressed as:
//   # * stamp.secs: seconds (stamp_secs) since epoch
//   # * stamp.nsecs: nanoseconds since stamp_secs
//   # time-handling sugar is provided by the client library
//   time stamp
//   #Frame this data is associated with
//   # 0: no frame
//   # 1: global frame
//   string frame_id
//   
// Instances of this class can be read and written with YARP ports,
// using a ROS-compatible format.

#ifndef YARPMSG_TYPE_std_msgs_Header
#define YARPMSG_TYPE_std_msgs_Header

#include <string>
#include <vector>
#include <yarp/os/Portable.h>
#include <yarp/os/ConstString.h>
#include <yarp/os/NetInt16.h>
#include <yarp/os/NetUint16.h>
#include <yarp/os/NetInt32.h>
#include <yarp/os/NetUint32.h>
#include <yarp/os/NetInt64.h>
#include <yarp/os/NetUint64.h>
#include <yarp/os/NetFloat32.h>
#include <yarp/os/NetFloat64.h>
#include <TickTime.h>

class std_msgs_Header : public yarp::os::Portable {
public:
  yarp::os::ConstString getTypeName() const {
    return "std_msgs_Header";
  }

  yarp::os::NetUint32 seq;
  TickTime stamp;
  std::string frame_id;

  bool read(yarp::os::ConnectionReader& connection) {
    // *** seq ***
    seq = connection.expectInt();

    // *** stamp ***
    if (!stamp.read(connection)) return false;

    // *** frame_id ***
    int len = connection.expectInt();
    frame_id.resize(len);
    if (!connection.expectBlock((char*)frame_id.c_str(),len)) return false;
    return !connection.isError();
  }

  bool write(yarp::os::ConnectionWriter& connection) {
    // *** seq ***
    connection.appendInt(seq);

    // *** stamp ***
    if (!stamp.write(connection)) return false;

    // *** frame_id ***
    connection.appendInt(frame_id.length());
    connection.appendExternalBlock((char*)frame_id.c_str(),frame_id.length());
    return !connection.isError();
  }
};

#endif
