// This is an automatically generated file.
// Generated from this humanoid_nav_msgs_StepTarget.msg definition:
//   # Target for a single stepping motion of a humanoid's leg
//   
//   geometry_msgs/Pose2D pose   # step pose as relative offset to last leg
//   uint8 leg                   # which leg to use (left/right, see below)
//   
//   uint8 right=0               # right leg constant
//   uint8 left=1                # left leg constant
//   
// Instances of this class can be read and written with YARP ports,
// using a ROS-compatible format.

#ifndef YARPMSG_TYPE_humanoid_nav_msgs_StepTarget
#define YARPMSG_TYPE_humanoid_nav_msgs_StepTarget

#include <string>
#include <vector>
#include <yarp/os/Portable.h>
#include <yarp/os/ConstString.h>
#include <yarp/os/NetInt16.h>
#include <yarp/os/NetUint16.h>
#include <yarp/os/NetInt32.h>
#include <yarp/os/NetUint32.h>
#include <yarp/os/NetInt64.h>
#include <yarp/os/NetUint64.h>
#include <yarp/os/NetFloat32.h>
#include <yarp/os/NetFloat64.h>
#include <TickTime.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/Pose2D.h>

class humanoid_nav_msgs_StepTarget : public yarp::os::Portable {
public:
  yarp::os::ConstString getTypeName() const {
    return "humanoid_nav_msgs_StepTarget";
  }

  geometry_msgs/Pose2D pose;
  unsigned char leg;
  unsigned char right=0;
  unsigned char left=1;

  bool read(yarp::os::ConnectionReader& connection) {
    // *** pose ***
    if (!pose.read(connection)) return false;

    // *** leg ***
    if (!connection.expectBlock((char*)&leg,1)) return false;

    // *** right=0 ***
    if (!connection.expectBlock((char*)&right=0,1)) return false;

    // *** left=1 ***
    if (!connection.expectBlock((char*)&left=1,1)) return false;
    return !connection.isError();
  }

  bool write(yarp::os::ConnectionWriter& connection) {
    // *** pose ***
    if (!pose.write(connection)) return false;

    // *** leg ***
    connection.appendBlock((char*)&leg,1);

    // *** right=0 ***
    connection.appendBlock((char*)&right=0,1);

    // *** left=1 ***
    connection.appendBlock((char*)&left=1,1);
    return !connection.isError();
  }
};

#endif
